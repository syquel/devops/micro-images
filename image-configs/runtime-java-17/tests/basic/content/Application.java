package local.example.test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.HttpServer;

public class Application {

	public static void main(final String[] args) throws IOException {
		final HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080), -1);

		httpServer.createContext(
			"/",
			exchange -> {
				final byte[] response = "Hello World!".getBytes(StandardCharsets.UTF_8);

				exchange.sendResponseHeaders(200, response.length);
				exchange.getResponseBody().write(response);
			}
		);

		httpServer.start();
	}

}
