#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh

readonly CONTAINER_IP="${1}"
readonly CONTAINER_PORT=8080
readonly CONTAINER_BASE_URL="http://${CONTAINER_IP}:${CONTAINER_PORT}"

log_info "Checking /"
RESPONSE_CONTENT="$( curl -sSfL "${CONTAINER_BASE_URL}/" )"
if [ "${RESPONSE_CONTENT}" != "Hello World!" ]; then
  log_error "Expected 'Hello World!' but got '${RESPONSE_CONTENT}'"
  exit 2
fi
