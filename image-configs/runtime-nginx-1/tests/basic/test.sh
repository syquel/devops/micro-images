#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh

readonly CONTAINER_IP="${1}"
readonly CONTAINER_BASE_URL="http://${CONTAINER_IP}:8080"

log_info "Checking /index.html"
RESPONSE_CONTENT="$( curl -sSfL "${CONTAINER_BASE_URL}/index.html" )"
if [ "${RESPONSE_CONTENT}" != "INDEX_HTML_MARKER" ]; then
  log_error "Expected 'INDEX_HTML_MARKER' but got '${RESPONSE_CONTENT}'"
  exit 2
fi

log_info "Checking /"
RESPONSE_CONTENT="$( curl -sSfL "${CONTAINER_BASE_URL}/" )"
if [ "${RESPONSE_CONTENT}" != "INDEX_HTML_MARKER" ]; then
  log_error "Expected 'INDEX_HTML_MARKER' but got '${RESPONSE_CONTENT}'"
  exit 2
fi

log_info "Checking /content.txt"
RESPONSE_CONTENT="$( curl -sSfL "${CONTAINER_BASE_URL}/content.txt" )"
if [ "${RESPONSE_CONTENT}" != "CONTENT_MARKER" ]; then
  log_error "Expected 'CONTENT_MARKER' but got '${RESPONSE_CONTENT}'"
  exit 2
fi

log_info "Checking /content.css content-type"
RESPONSE_CONTENT="$( curl -sSfL -o /dev/null --write-out "%{content_type}" "${CONTAINER_BASE_URL}/content.css" )"
if [ "${RESPONSE_CONTENT}" != "text/css" ]; then
  log_error "Expected 'CONTENT_CSS_MARKER' but got '${RESPONSE_CONTENT}'"
  exit 2
fi
