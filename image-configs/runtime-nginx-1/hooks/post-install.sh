#!/usr/bin/env bash
set -euf -o pipefail

# Redirect log files to stdout and stderr
ln -vsf /dev/stdout "${CONTAINER_MOUNT}/var/log/nginx/access.log"
ln -vsf /dev/stderr "${CONTAINER_MOUNT}/var/log/nginx/error.log"
chmod -v 755 "${CONTAINER_MOUNT}/var/log/nginx"

# Make /run directory accessible
chmod -v 777 "${CONTAINER_MOUNT}/run"

# Make /var/lib/nginx accessible
chown -R "${RUNTIME_USER_ID}:${RUNTIME_USER_ID}" "${CONTAINER_MOUNT}/var/lib/nginx"
