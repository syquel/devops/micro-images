#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh
source scripts/utils.sh

readonly TEST_EXECUTABLE="test.sh"
readonly CONTENT_DIR="content"
readonly TESTS_BASE_DIR="tests"
readonly TEST_CONFIG_FILE_NAME="test.conf"

## Build config
OPTIONS=( "${@:1:${#}-1}" )
IMAGE_CONFIG_NAME="${*: -1}"

IMAGE_CONFIG_PATH="$( get_image_config_path "${IMAGE_CONFIG_NAME}" )"
declare -A config

# Parse config file
parse_build_config config "${IMAGE_CONFIG_NAME}"

# Parse commandline arguments
parse_build_options config "${OPTIONS[@]}"

if [[ ! -v config["runtimeImageName"] ]]; then
  log_error "Usage: ${0} [--runtime-image-name <RUNTIME_IMAGE_NAME>] [--runtime-port <RUNTIME_PORT>...] <IMAGE_CONFIG_NAME>"
  exit 3
fi

## Log system state
log_begin_section "system_info" "System information"

log_info "Kernel: $(uname -a)"

log_info "Podman:"
podman info

log_end_section "system_info"

## Test image
log_begin_section "image_test" "Testing image"

TESTS_PATH="$( realpath -m "${IMAGE_CONFIG_PATH}/${TESTS_BASE_DIR}" )"
if [ ! -d "${TESTS_PATH}" ]; then
  log_info "No tests available for image config ${IMAGE_CONFIG_NAME}"
  log_end_section "image_test"
  exit 0
fi

declare -a RUNTIME_PORTS
if [[ -v config["runtimePorts"] ]]; then
  split RUNTIME_PORTS " " "${config["runtimePorts"]}"
fi

declare -a FAILED_TESTS=( )
while read -d $'\0' -r test_dir; do
  TEST_NAME="$( basename "${test_dir}" )"

  log_info "Executing test: ${TEST_NAME}"
  if [ ! -e "${test_dir}/${TEST_EXECUTABLE}" ]; then
    log_error "Test is missing ${TEST_EXECUTABLE}"
    continue
  fi

  # Read test specific configuration
  declare -a TEST_RUNTIME_PORTS=( "${RUNTIME_PORTS[@]}" )

  TEST_CONFIG_PATH="${test_dir}/${TEST_CONFIG_FILE_NAME}"
  declare -A testConfig
  if [ -e "${TEST_CONFIG_PATH}" ]; then
    parse_config testConfig "${TEST_CONFIG_PATH}"

    # Add exposed ports from test config
    declare -a TEST_RUNTIME_PORTS
    if [[ -v testConfig["runtimePorts"] ]]; then
      split TEST_RUNTIME_PORTS " " "${testConfig["runtimePorts"]}"
    fi
  fi

  log_info "Starting container for image ${config["runtimeImageName"]}"
  # Compile Podman arguments
  PODMAN_ARGS=(
    --cap-drop=ALL --cgroupns=private --image-volume=bind --network=private --pull=never --read-only --read-only-tmpfs=true --systemd=false --userns=auto
    --tty
  )
  for exposed_port in "${TEST_RUNTIME_PORTS[@]}"; do
    PODMAN_ARGS+=( "--expose=${exposed_port}" )
  done
  if [ -d "${test_dir}/${CONTENT_DIR}" ]; then
    PODMAN_ARGS+=( "--volume=${test_dir}/${CONTENT_DIR}:/app:ro" )
  fi
  if [[ -v testConfig["runtimeEntrypoint"] ]]; then
    PODMAN_ARGS+=( --entrypoint "${testConfig["runtimeEntrypoint"]}" )
  fi

  # Start container
  CONTAINER_ID="$( podman create --rm "${PODMAN_ARGS[@]}" "${config["runtimeImageName"]}" )"
  podman start --attach "${CONTAINER_ID}" &
  CONTAINER_PID="${!}"

   # Check if container is running
  if ! podman container exists "${CONTAINER_ID}"; then
    log_error "Container could not be created"
    exit 7
  fi

  if ! timeout 10s podman wait --condition="running" "${CONTAINER_ID}" > /dev/null; then
    CONTAINER_STATUS="$( podman container inspect --format "{{.State.Status}}" "${CONTAINER_ID}" )"
    log_error "Failed to start container. Container in state ${CONTAINER_STATUS}"
    exit 6
  fi

  # Acquire container IP
  CONTAINER_IP="$( podman container inspect --format "{{.NetworkSettings.IPAddress}}" "${CONTAINER_ID}" )"

  # Wait until Container is up if an exposed port has been specified
  if [ ${#TEST_RUNTIME_PORTS[@]} -gt 0 ]; then
    log_info "Waiting for container to be up"

    if [[ "${TEST_RUNTIME_PORTS[0]}" =~ ^([[:digit:]]+)"/"([[:alpha:]]+)$ ]]; then
      CONNECTION_PORT=${BASH_REMATCH[1]}
      CONNECTION_TYPE=${BASH_REMATCH[2]}
    else
      CONNECTION_PORT="${TEST_RUNTIME_PORTS[0]}"
      CONNECTION_TYPE="tcp"
    fi

    declare -i CONNECTION_ATTEMPT=0
    while true; do
      CONNECTION_ATTEMPT+=1

      # Open TCP connection to container
      log_info "Check container availability via ${CONNECTION_TYPE} port ${CONNECTION_PORT}"
      if true 2> /dev/null > "/dev/${CONNECTION_TYPE}/${CONTAINER_IP}/${CONNECTION_PORT}"; then
        break
      fi

      # Check if retry count has been exceeded
      if [ ${CONNECTION_ATTEMPT} -ge 5 ]; then
        log_error "Failed container up check"
        exit 2
      fi

      # Wait before retrying
      sleep 1
    done
  fi

  # Execute test
  if ! "${test_dir}/${TEST_EXECUTABLE}" "${CONTAINER_IP}" "${CONTAINER_ID}"; then
    log_error "Test failed: ${TEST_NAME}"
    FAILED_TESTS+=( "${TEST_NAME}" )
  fi

  # Remove container
  log_info "Stopping container"
  podman stop --time 10 "${CONTAINER_ID}"
done < <(find "${TESTS_PATH}" -mindepth 1 -maxdepth 1 -type d -print0)

if [ "${#FAILED_TESTS[@]}" -gt 0 ]; then
  log_error "Failed tests: ${FAILED_TESTS[*]}"
  exit 5
fi

log_end_section "image_test"
