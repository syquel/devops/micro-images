#!/usr/bin/env bash
set -euf -o pipefail

readonly COLOR_RESET="\e[0m"
readonly COLOR_BLUE="\e[34m"
readonly COLOR_BLUE_LIGHT="\e[94m"
readonly COLOR_RED="\e[31m"

function log_begin_section() {
  local sectionId="${1}"
  local sectionTitle="${2}"

  echo -e "\e[0Ksection_start:`date +%s`:${sectionId}\r\e[0K${COLOR_BLUE_LIGHT}${sectionTitle}${COLOR_RESET}"
}

function log_end_section() {
  local sectionId="${1}"

  echo -e "\e[0Ksection_end:`date +%s`:${sectionId}\r\e[0K"
}

function log_info() {
  local message="${1}"

  echo -e "${COLOR_BLUE}${message}${COLOR_RESET}"
}

function log_error() {
  local message="${1}"

  echo -e "${COLOR_RED}${message}${COLOR_RESET}" 1>&2
}
